#include <iostream>

using namespace std;
/*
Description de la funcion:

Esta es la funcion main de nuestro programa, es la clase WS Taller 
Dirigida por:
*/

int main(){
	
	//Declaration of variables
	int myFirstVariable;
	float myFirstFloat;
	double myFirstDouble;
	char myFirstChar;
	
	//Initialize
	myFirstVariable = 1;
	myFirstFloat = 2.512345678;
	myFirstDouble = 2.512345678;
	myFirstChar= 'h';
	
	//Display of the variable
	cout<<myFirstFloat;

	
	return 0;
}
