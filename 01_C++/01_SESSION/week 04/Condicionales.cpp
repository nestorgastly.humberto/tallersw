

//=============================================================INCLUDE===========================================
#include <iostream>
using namespace std;
//=========================================================GLOBAL VARIABLE==========================================================
int inputCurrentTimeHour = 0;
int inputPersonAge = 0;
bool inputHallSpecialPass=0;
//=========================================================FUNCTION DECLARATION============================================
void Run();
void CollectData();
void ShowResult();
bool CheckHour(int hour, int minHour, int maxHour);
bool CheckAge(int age, int minAge, int maxAge);
bool CheckSpecialPass (bool specialPass)
//========================================MAIN=========================================================================

int main(){
	Run();
	return 0;
}
//=============================================FUNCTION DEFINITION=========================================================================
void Run(){
	CollectData();
	CheckAccess();
	ShowResult();
}
//===================================================================================================
void CollectData(){
	cout<<"=============Insert Data==========";
	cout<<"Ingrese la hora: ";
	cin>>inputCurrentTimeHour;
	cout<<"Ingrese la edad de la persona: a";
	cin>>inputPersonAge;
	
	int tempHallSpecialPass = 0;
	cout<<"Ingrese si cuenta con el Special Pass (No=0, Si=1)";
	cin>> tempHallSpecialPass;
	
	if (tempHallSpecialPass ==1){
		inputHallSpecialPass = true;
	} else {
		inputHallSpecialPass = false;
	}
	
}
//=====================================================================================================
void CheckAccess(){
		
	if (CheckHour(inputCurrentTimeHour,4,24) ){
		if (CheckAcces(inputPersonAge,18,64)  {
		cout<<"Ingreso permitido\r\n";
		cout<<"Bienvenido";
		} else {
			if(hallSpecialPass == true){
				cout << "Ingreso permitido con Special Pass\r\n";
				cout << "Bienvenido\r\n";
			} else{
			cout<<"Ingreso no permitido: Toque de queda\r\n";
			}
		}
	}
}
//===============================================================================================================
bool CheckHour (int hour, int minHour, int maxHour){
	if (hour >= minHour && hour <=maxHour)
		return true;
	} else {
		return false;
	}
}
//===========================================================================================
bool CheckSpecialPass(bool specialPass){
	return specialPass;
}
