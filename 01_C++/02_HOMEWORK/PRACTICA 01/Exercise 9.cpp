//Algorithm to calculate the capital
/*
@nestorgastly.humberto(nestorgastly.humberto@gmail.com
@Date 28/01/2022
/*****************************************************************************INCLUDE**********************************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <math.h>
using namespace std;
/*
Exercise 9
*/
//***************************************************************************FUNCTION DECLARATION**********************************************************************************
void Run();
void CollectData();
void Calculate();
void ShowResults();
/*******************************************************************MAIN*****************************************************************************************************************/
int main() {
	
	Run();
	return 0;
	}
//***********************************************************************GLOBAL VARIABLE*********************************************************************************************************
	double cap_fin;
	double tasa;
	double tiempo;
	double int_comp;
	double cap_ini;
/********************************************************************FUNCTION DEFINITION**********************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//======================================================================================================================================================================================
void CollectData(){
    cout << "Ingresa el valor de capital final: ";
    cin >> cap_fin;
    cout << "Ingresa el valor de tasa de interes: ";
    cin >> tasa;
    cout << "Ingresa el valor de tiempo: ";
    cin >> tiempo;
}
//===================================================================================================================================================================================
void Calculate() {
    cap_ini=cap_fin/(pow(1.0+tasa/100,tiempo));
}
//=========================================================================================================================================================================================================================	
void ShowResults(){
    cout << "Valor de capital inicial: " << cap_ini << endl;
}
