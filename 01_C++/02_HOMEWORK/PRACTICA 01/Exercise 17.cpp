//Algorithm for calculating the area and perimeter of an ellipse
/*
@nestorgastly.humberto(nestorgastly.humberto@gmail.com)
@Date 4/12/2021
*/

/*******************************************************************INCLUDE*******************************************************************************************************/
#include <iostream>

using namespace std;
/*
Exercise 17
*/
//Global variable
double pi = 3.1415;
/***********************************************************************************MAIN****************************************************************************************/
int main(){
	//Declaration
	double radio =2.5; //Minor radius of ellipse
	double Radio = 6.4; //Major radius of ellipse
	double perim; //Perimeter
	double area;//Area
	
	//Operador
	area = (pi)*(radio)*(Radio);
	perim = (pi)*8*(radio+Radio)*0.5;
	
	//Display phrase 1
	cout<<"El area es: "<< area;
	cout<<"El perimetro es: "<< perim;
	return 0;
}

