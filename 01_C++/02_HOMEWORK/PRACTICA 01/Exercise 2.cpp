//Algorithm for calculate the percentage of students
/*
@nestorgastly.humberto(nestorgastly.humberto@gmail.com)
@Date 4/12/2021
*/
/********************************************************INCLUDE*********************************************************/
#include <iostream>
#include <cmath>
using namespace std;
/*
Exercise 2
*/
/**********************************************MAIN**********************************************************************/
int main() {
/*******************************DECLARATION******************************************************************************/
	int varon=0.0; //Cantidad de varones 
	int mujer=0.0; //Cantidad de mujeres
	int salon=0.0; //Cantidad de alumnos
	float ph=0.0; //Porcentaje de varones
	float pm=0.0; //Porcentaje de mujeres

	//Display phrase 1
	cout<<"Cantidad de hombres en el salon: ";
	cin>>varon;
	cout<<"Cantidad de mujeres en el salon: ";
	cin>>mujer;
	
	//Operador
	salon =(varon)+(mujer);
	
	//Operador porcentaje
	ph = (double(varon)*100)/(salon);
	pm = (double(mujer)*100)/(salon);
	
	//Display phrase 2
	cout<<"El porcentaje de hombres son: ";
	cout<<ph; 
	cout<<"%";
	cout<<"\n\rEl porcentaje de mujeres son: ";
	cout<<pm; 
	cout<<"%";
	return 0;
}

