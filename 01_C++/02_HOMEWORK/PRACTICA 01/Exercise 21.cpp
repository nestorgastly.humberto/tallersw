//Algorithm for calculating velocity
/*
@nestorgastly.humberto(nestorgastly.humberto@gmail.com)
@Date 4/12/2021
*/

/**************************************************************INCLUDE*************************************************************************************************************/
#include <iostream>

using namespace std;
/*
Exercise 21
*/
/***********************************************************************************MAIN******************************************************************************************/
int main(){
	//Declaration
	float h; //Hours
	float km; //Kilometres
	double velocidad; //Velocity
	
	//Display phrase 1
	cout<<"Inserte la distancia recorrida (en km): ";
	cin>>km;
	cout<<"Inserte el tiempo del recorrido (en horas): ";
	cin>>h;
	
	//Operador
	velocidad = (km)/(h);
	
	//Display phrase 2
	cout<<"Se desplaza con una velocidad de: ";
	cout<<velocidad;
	cout<< " km/h";
}
