//Algorithm for calculating the area and volume of a cylinder
/*
@nestorgastly.humberto(nestorgastly.humberto@gmail.com)
@Date 5/12/2021
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416		// Constante Pi 

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalInputHeight = 0.0; 		// Entrada de altura a traves del terminal
float terminalInputRadius = 0.0;	 	// Entrada de radio a traves del terminal

float circularArea = 0.0;				// Area circular calculada
float lateralAreaCylinder = 0.0;		// Area lateral del cilindro calculada
float volumeCylinder = 0.0;				// Volumen del cilindro calculada

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateCircularArea(float radius);
float CalculateLateralAreaCylinder(float radius, float height);
float CalculateVolumeCylinder(float radius, float height);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tEscribe el radio del cilindro: ";
	cin>>terminalInputRadius;
	cout<<"\tEscribe la altura del cilindro: ";
	cin>>terminalInputHeight;
}
//=====================================================================================================

void Calculate(){
	circularArea = CalculateCircularArea(terminalInputRadius);
	lateralAreaCylinder = CalculateLateralAreaCylinder(terminalInputRadius, terminalInputHeight);
	volumeCylinder = CalculateVolumeCylinder(terminalInputRadius, terminalInputHeight);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tEl area circular es: "<< circularArea<<"\r\n";
	cout<<"\tEl area lateral es: "<<lateralAreaCylinder<<"\r\n";
	cout<<"\tEl volumen es: "<<volumeCylinder<<"\r\n";
}
//=====================================================================================================

float CalculateCircularArea(float radius){
	return PI * pow(radius,2.0);	// area_circulo = pi*radio^2
}
//=====================================================================================================

float CalculateLateralAreaCylinder(float radius, float height){
	return 2.0 * PI * radius * height;	// area lateral = 2 * pi * radio * h
}

//=====================================================================================================
float CalculateVolumeCylinder(float radius, float height){
	return PI * pow(radius,2.0) * height;
}

 

